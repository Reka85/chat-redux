// external modules
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { logger } from 'redux-logger';
import reduxPromise from 'redux-promise';

// internal modules
import App from './components/app';
import '../assets/stylesheets/application.scss';

// State and reducers
import messagesReducer from "./reducers/messages_reducer";
import channelsReducer from "./reducers/channels_reducer";
import selectedChannelReducer from "./reducers/selected_channel_reducer";
import currentUserReducer from "./reducers/current_user_reducer";

const reducers = combineReducers({
  messages: messagesReducer,
  channels: channelsReducer,
  selectedChannel: selectedChannelReducer,
  currentUser: currentUserReducer
});

const initialState = {
  messages: [],
  channels: ['general', 'react', 'rails', 'paris'],
  selectedChannel: 'general',
  currentUser: prompt("What is your name?") || `anonymous${Math.floor(10 + (Math.random() * 90))}`
}

const middlewares = applyMiddleware(reduxPromise, logger);

ReactDOM.render(
  <Provider store={createStore(reducers, initialState, middlewares)}>
    <App />
  </Provider>,
  document.getElementById('root')
);
